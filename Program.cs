﻿using System;
using System.Threading.Tasks;

namespace consolecaddyparser
{
  class Program
  {
    static async Task Main(string[] args)
    {
      string logFilePath = args.Length > 0 ? args[0] : "";

      if (logFilePath.Equals(""))
      {
        Console.WriteLine("You have not provided anything as a command line argument.\n"
          + "Run the application with `dotnet run /caddy/server/logFile/path`");
      }
      else
      {
        var results = await CaddyFileReader.DisplayUris(logFilePath);
        if (results != null)
        {
          foreach (var uri in results)
          {
            string time = uri.Value == 1 ? " time." : " times.";
            Console.WriteLine(uri.Key + " was visited " + uri.Value + time);
          }
        }
      }
    }
  }
}
