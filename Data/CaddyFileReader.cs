using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace consolecaddyparser
{
  ///<summary>
  /// Class reads the contents of a given file and attempts to parse URIs and 
  /// the count of IPs that have visited them returning a SortedDictionary<uriString, visitCount>
  ///</summary>
  public static class CaddyFileReader
  {
    public static async Task<SortedDictionary<string, int>> DisplayUris(string filePath)
    {
      List<string> fileContents = new List<string>();
      SortedDictionary<string, int> visitors = new SortedDictionary<string, int>();
      await Task.Run(async () =>
      {
        // read the log file on the given path and save values into a list of strings
        await using (FileStream SourceStream = File.Open(
          filePath,
          FileMode.Open,
          FileAccess.Read,
          FileShare.ReadWrite))
        {
          try
          {
            byte[] logFileContents = new byte[SourceStream.Length];
            await SourceStream.ReadAsync(logFileContents, 0, (int)SourceStream.Length);
            fileContents = System.Text.Encoding.ASCII.GetString(logFileContents)
              .Trim()
              .Split('\n')
              .ToList();
          }
          catch (Exception ex)
          {
            System.Console.WriteLine($"Something went wrong reading the log file! - {ex.Message}");
          }
        }

        // Parse strings in list and get URIs and IP count that visited the URI
        foreach (var row in fileContents)
        {
          try
          {
            using (JsonDocument document = JsonDocument.Parse(row))
            {
              JsonElement root = document.RootElement;
              JsonElement requestElement = root.GetProperty("request");
              JsonElement uriElement = requestElement.GetProperty("uri");
              var uri = uriElement.GetString();
              // all useful URLs on my page end with '/' so this weeds out all the spam
              if (uri.EndsWith("/"))
              {
                if (!visitors.ContainsKey(uri) && uri.Length == 1
                    || !visitors.ContainsKey(uri) && uri.Contains("posts"))
                {
                  // a bit hacky and unreliable - what if this pattern changes?
                  string uriToFind = $"\"uri\":\"{uri}\"";
                  visitors.Add(
                    uri,
                    fileContents.Count(x => x.Contains(uriToFind))
                  );
                }
              }
            }
          }
          catch (Exception e)
          {
            System.Console.WriteLine($"Couldn't parse entry." +
              $"\n->Entry value: {row} " +
              $"\n->Error message: {e.Message}" + 
              $"\n>>>=======================================================>>>");
          }
        }
      });

      return visitors;
    }
  }
}