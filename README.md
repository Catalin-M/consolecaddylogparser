# Console Caddy Log File Parser  

This console application will read an unmodified [Caddy](https://caddyserver.com/) server log file passed in as a parameter and output to the console the visited urls.

Because of the way I wrote it for use with my Hugo website, it looks at a particular string pattern in the URL to filter out attacks/fishing on different webserver URLS. Feel free to modify those checks to suit your URL patterns. Hugo pages will have the same patterns though in general, so it should work unmodified for that purpose.  

To test out the code, install git, install .NET 5 (or 6) and run the application.  

Install steps:  

## 1. Install Git

`sudo apt install git-all`  

## 2. Install the .NET SDK on Ubuntu (search for different version you might be running on the Microsoft documentation page):  

- Add Microsoft package signing key:  

```bash
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
```  

- Install the SDK  
```bash
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-6.0
```  
You can confirm the install completed by using `dotnet --version`  

## 3. Clone the repo:  

```bash
git clone https://gitlab.com/Catalin-M/consolecaddylogparser.git
```  

## 4. CD into the folder and run the application:  

```bash
cd consolecaddylogparser
dotnet run $path/to/your/caddy.log
```  

The important part is the filepath. There aren't any special permissions that are needed for the app to read the console.  

Below is a screenshot showing the output for my hugo page.  

![Console Caddy Log Output](consolecaddy.PNG)
